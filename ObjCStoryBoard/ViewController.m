//
//  ViewController.m
//  ObjCStoryBoard
//
//  Created by Lyndy Tankersley on 2/2/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIStepper *stepper1;
@property (weak, nonatomic) IBOutlet UIStepper *stepper2;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)stepper1:(id)sender {
    _lbl1.text = [NSString stringWithFormat: @"%i", (int)_stepper1.value];
}
- (IBAction)stepper2:(id)sender {
    _lbl2.text = [NSString stringWithFormat: @"%i", (int)_stepper2.value];
}
- (IBAction)resetBtn:(id)sender {
    _lbl1.text = @"0";
    _lbl2.text = @"0";
    _stepper1.value = 0;
    _stepper2.value = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
